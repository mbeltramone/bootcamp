@extends('layouts.app')

@section('content')
	<div class="container">
		<form method="POST" action="/books">
			@csrf
            <div class="form-group text-center">
                <h2>Cargar Libros</h2>
            </div>

            <div class="form-group" style="text-align: left">
                <label class="text-left" for="name">Nombre: </label>
                <input type="text" class="form-control" id="name" name="name">
            </div>

            <div class="form-group" style="text-align: left">
                <label class="text-left" for="name">Autor: </label>

                <select class="form-control" name="author_id">
                    @foreach($authors as $author)
                        <option value="{{$author->id}}">
                            {{$author->getName()}}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="form-group" style="text-align: left">
                <label class="text-left" for="name">Descripción: </label>

                <textarea class="form-control" name="description"></textarea>
            </div>
			<button type="submit" class="btn btn-primary">Guardar</button>
		</form>
	</div>
@endsection
