@extends('layouts.app')

@section('content')
    <div>
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Author Name</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($authors as $author)
                <tr>
                    <th scope="row">{{$author->getId()}}</th>
                    <td>{{ $author->getName() }}</td>
                    <td class="justify-content-center" style="display: flex">
                        <a class="btn btn-info btn-xs" href="/authors/{{ $author->getId() }}/edit">
                            <span class="text-white">Editar</span>
                        </a>

                        <form method="POST" action="/authors/{{ $author->getId() }}">
                            @csrf
                            @method('DELETE')

                            <button type="submit" class="btn btn-danger btn-xs">
                                <span>Eliminar</span>
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <a class="btn btn-primary w-100" href="/authors/create">
            <span>Agregar</span>
        </a>
    </div>
@endsection
