<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Book extends Model
{
    public function getId(): int
    {
        return $this->id;
    }

    public function setName(string $name): void
    {
    	$this->name = $name;
    }

    public function getName(): string
    {
    	return $this->name;
    }

    public function setDescription(string $description): void
    {
    	$this->description = $description;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setAuthor(Author $author): void
    {
    	$this->author()->associate($author);
    }

    public function getAuthor(): Author
    {
        return $this->author;
    }

    public function author(): BelongsTo
    {
        return $this->belongsTo(Author::class);
    }

    public function reservations(): HasMany
    {
        return $this->hasMany(Reservation::class);
    }
}
