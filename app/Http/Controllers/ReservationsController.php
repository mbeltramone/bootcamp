<?php

namespace App\Http\Controllers;

use App\Book;
use App\Reservation;
use App\User;
use DateTimeImmutable;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ReservationsController extends Controller
{
    public function index(): View
    {
        $reservations = Reservation::all();

        return view('reservations.index')
            ->with('reservations', $reservations);
    }

    public function create(): View
    {
        $users = User::all();
        $books = Book::all();

        return view('reservations.create')
            ->with('users', $users)
            ->with('books', $books);
    }

    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'user_id' => 'required',
            'book_id' => 'required',
            'return_date' => 'required'
        ]);

        $book = Book::findOrFail($request->get('book_id'));
        $user = User::findOrFail($request->get('user_id'));

        $reservation = new Reservation();

        $reservation->setBook($book);
        $reservation->setUser($user);
        $reservation->setReturnDate(new DateTimeImmutable($request->get('return_date')));

        $reservation->save();

        return redirect()->to('/reservations');
    }

    public function edit(int $id): View
    {
        $reservation = Reservation::findOrFail($id);

        $users = User::all();
        $books = Book::all();

        return view('reservations.edit')
            ->with('reservation', $reservation)
            ->with('users', $users)
            ->with('books', $books);
    }

    public function update(Request $request, int $id): RedirectResponse
    {
        $reservation = Reservation::findOrFail($id);

        $user = User::findOrFail($request->get('user_id'));
        $book = Book::findOrFail($request->get('book_id'));

        $reservation->setBook($book);
        $reservation->setUser($user);
        $reservation->setReturnDate(new DateTimeImmutable($request->get('return_date')));

        $reservation->update();

        return redirect()
            ->to('/reservations')
            ->with('message', 'La reservación se actualizo correctamente');
    }

    public function destroy(int $id): RedirectResponse
    {
        $reservation = Reservation::findOrFail($id);

        $reservation->delete();

        return redirect()
            ->to('/reservations')
            ->with('message', 'La reservación se ha eliminado correctamente');
    }
}
