<?php

namespace App;

use DateTime;
use DateTimeImmutable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use InvalidArgumentException;

class Reservation extends Model
{
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->setStatus(self::STATUS_BORROWED);
    }

    public const AVAILABLE_STATUSES = [
        self::STATUS_BORROWED,
        self::STATUS_RETURNED
    ];

    public const STATUS_RETURNED = 'returned';
    public const STATUS_BORROWED = 'borrowed';

    public function getId(): int
    {
        return $this->id;
    }

    public function setUser(User $user): void
    {
        $this->user()->associate($user);
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setBook(Book $book): void
    {
        $this->book()->associate($book);
    }

    public function getBook(): Book
    {
        return $this->book;
    }

    public function setReturnDate(DateTimeImmutable $returnDate): void
    {
        $this->return_date = $returnDate;
    }

    public function getReturnDate(): string
    {
        return substr($this->return_date, 0, 10);
    }

    public function setStatus(string $status): void
    {
        if (
            !in_array(
                $status,
                self::AVAILABLE_STATUSES
            )
        ) {
            throw new InvalidArgumentException(
                'Status: ' . $status . ' is not available'
            );
        }

        $this->status = $status;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function book(): BelongsTo
    {
        return $this->belongsTo(Book::class);
    }
}
