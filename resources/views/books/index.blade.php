@extends('layouts.app')

@section('content')
    <div>
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Description</th>
                <th scope="col">Autor</th>
                <th scope="col">Acciones</th>
            </tr>
            </thead>
            <tbody>
                @foreach($books as $book)
                    <tr>
                        <th scope="row">{{$book->getId()}}</th>
                        <td>{{ $book->getName() }}</td>
                        <td>{{ mb_strimwidth($book->getDescription(), 0, 30, "...") }}</td>
                        <td>{{ $book->getAuthor()->getName() }}</td>
                        <td class="justify-content-around" style="display: flex">
                            <a class="btn btn-info btn-xs" href="/books/{{ $book->getId() }}/edit">
                                <span class="text-white">Editar</span>
                            </a>

                            <form method="POST" action="/books/{{ $book->getId() }}">
                                @csrf
                                @method('DELETE')

                                <button type="submit" class="btn btn-danger btn-xs">
                                    <span>Eliminar</span>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <a class="btn btn-primary w-100" href="/books/create">
            <span>Agregar</span>
        </a>
    </div>
@endsection
