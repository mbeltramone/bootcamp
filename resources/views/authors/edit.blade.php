@extends('layouts.app')

@section('content')
    <div class="container">
        <form method="POST" action="/authors/{{ $author->getId() }}">
            @csrf
            @method('PUT')

            <div class="form-group text-center">
                <h2>Editar Autor</h2>
            </div>
            <div class="form-group" style="text-align: left">
                <label class="text-left" for="name">Nombre: </label>
                <input type="text" class="form-control" id="name" name="name" value="{{ $author->getName() }}">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Actualizar</button>
            </div>
        </form>
    </div>
@endsection
