@extends('layouts.app')

@section('content')
	<div class="container">
		<form method="POST" action="/authors">
			@csrf
            <div class="form-group text-center">
                <h2>Cargar Autor</h2>
            </div>
            <div class="form-group" style="text-align: left">
                <label class="text-left" for="name">Nombre: </label>
                <input type="text" class="form-control" id="name" name="name">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
		</form>
	</div>
@endsection
