@extends('layouts.app')

@section('content')
    <div class="wrapper">
        <!-- Sidebar -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3>Sistema de Bibliotecas</h3>
            </div>

            <ul class="list-unstyled components">
                <p>Kiwing Academy</p>
                <li class="active">
                    <a href="#authorSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Autores</a>
                    <ul class="collapse list-unstyled" id="authorSubmenu">
                        <li>
                            <a href="/authors">Listado de Autores</a>
                        </li>
                        <li>
                            <a href="/authors/create">Cargar Autor</a>
                        </li>
                    </ul>
                </li>
                <li class="active">
                    <a href="#bookSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Libros</a>
                    <ul class="collapse list-unstyled" id="bookSubmenu">
                        <li>
                            <a href="/books">Listado de Libros</a>
                        </li>
                        <li>
                            <a href="/books/create">Cargar Libro</a>
                        </li>
                    </ul>
                </li>
                <li class="active">
                    <a href="#reservationSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Reservaciones</a>
                    <ul class="collapse list-unstyled" id="reservationSubmenu">
                        <li>
                            <a href="/reservations">Listado de Reservaciones</a>
                        </li>
                        <li>
                            <a href="/reservartions/create">Cargar Reservacion</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
@endsection
