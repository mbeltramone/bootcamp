<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Book;
use App\Author;
use Illuminate\View\View;

class BooksController extends Controller
{
    public function index(): View
    {
    	$books = Book::all();

    	return view('books.index')
    		->with('books', $books);
    }

    public function create(): View
    {
        $authors = Author::all();

        return view('books.create')
            ->with('authors', $authors);
    }

    public function store(Request $request): RedirectResponse
    {
    	$book = new Book();

    	$author = Author::findOrFail($request->get('author_id'));

    	$book->setName($request->get('name'));
    	$book->setDescription($request->get('description'));
    	$book->setAuthor($author);

    	$book->save();

    	return redirect()->to('/books');
    }

    public function edit(int $id): View
    {
        $book = Book::findOrFail($id);
        $authors = Author::all();

        return view('books.edit')
            ->with('book', $book)
            ->with('authors', $authors);
    }

    public function update(Request $request, int $id): RedirectResponse
    {
        $book = Book::findOrFail($id);

        $author = Author::findOrFail($request->get('author_id'));

        $book->setName($request->get('name'));
        $book->setDescription($request->get('description'));
        $book->setAuthor($author);

        $book->update();

        return redirect()
            ->to('/books')
            ->with('message', 'El libro se actualizo correctamente');
    }

    public function destroy(int $id): RedirectResponse
    {
        $book = Book::findOrFail($id);

        $book->delete();

        return redirect()
            ->to('/books')
            ->with('message', 'El libro se ha eliminado correctamente');
    }
}
