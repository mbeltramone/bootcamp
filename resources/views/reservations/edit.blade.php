@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="form-group text-center">
            <h2>Cargar Reservaciones</h2>
        </div>

        <form method="POST" action="/reservations/{{ $reservation->getId() }}">
            @csrf
            @method('PUT')

            <div class="form-group" style="text-align: left">
                <label class="text-left" for="name">Cliente: </label>

                <select class="form-control" name="user_id">
                    @foreach($users as $user)
                        <option value="{{ $user->id }}" {{$reservation->getUser()->getId() === $user->getId() ? 'selected' : ''}}>
                            {{ $user->name }}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="form-group" style="text-align: left">
                <label class="text-left" for="name">Libro: </label>

                <select class="form-control" name="book_id">
                    @foreach($books as $book)
                        <option value="{{ $book->id }}" {{$reservation->getBook()->getId() === $book->getId() ? 'selected' : ''}}>
                            {{ $book->getName() }}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="form-group" style="text-align: left">
                <label class="text-left" for="name">Fecha de Devolución: </label>

                <input class="form-control" type="date" name="return_date" value="{{ $reservation->getReturnDate() }}">
            </div>

            <button type="submit" class="btn btn-primary">Guardar</button>
        </form>
    </div>
@endsection
