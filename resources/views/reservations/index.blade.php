@extends('layouts.app')

@section('content')
    <div>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Cliente</th>
                    <th scope="col">Libro
                    <th scope="col">Fecha devolución</th>
                    <th scope="col">Estado</th>
                    <th scope="col">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($reservations as $reservation)
                    <tr>
                        <th scope="row">{{ $reservation->getId() }}</th>
                        <td>{{ $reservation->getUser()->getName() }}</td>
                        <td>{{ $reservation->getBook()->getName() }}</td>
                        <td>{{ $reservation->getReturnDate() }}</td>
                        <td>{{ $reservation->getStatus() }}</td>

                        <td class="justify-content-around" style="display: flex">
                            <a class="btn btn-info btn-xs" href="/reservations/{{ $reservation->getId() }}/edit">
                                <span class="text-white">Editar</span>
                            </a>

                            <form method="POST" action="/reservations/{{ $reservation->getId() }}">
                                @csrf
                                @method('DELETE')

                                <button type="submit" class="btn btn-danger btn-xs">
                                    <span>Eliminar</span>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <a class="btn btn-primary w-100" href="/reservations/create">
            <span>Agregar</span>
        </a>
    </div>
@endsection
