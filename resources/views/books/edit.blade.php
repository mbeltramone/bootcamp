@extends('layouts.app')

@section('content')
    <div class="container">
        <form method="POST" action="/books/{{ $book->getId() }}">
            @csrf
            @method('PUT')

            <div class="form-group text-center">
                <h2>Editar Libros</h2>
            </div>

            <div class="form-group" style="text-align: left">
                <label class="text-left" for="name">Nombre: </label>
                <input type="text" class="form-control" id="name" name="name" value="{{ $book->getName() }}">
            </div>

            <div class="form-group" style="text-align: left">
                <label class="text-left" for="name">Autor: </label>

                <select class="form-control" name="author_id">
                    @foreach($authors as $author)
                        <option value="{{$author->id}}" {{$author->getId() === $book->getAuthor()->getId() ? 'selected' : ''}}>
                            {{$author->getName()}}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="form-group" style="text-align: left">
                <label class="text-left" for="name">Descripción: </label>

                <textarea class="form-control" name="description">{{ $book->getDescription() }}</textarea>
            </div>

            <button type="submit" class="btn btn-primary">Guardar</button>
        </form>
    </div>
@endsection
