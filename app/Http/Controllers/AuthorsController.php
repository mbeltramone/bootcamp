<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Author;
use Illuminate\View\View;

class AuthorsController extends Controller
{
    public function index(): View
    {
    	$authors = Author::all();

    	return view('authors.index')
    		->with('authors', $authors);
    }

    public function create(): View
    {
    	return view('authors.create');
    }

    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'name' => 'required',
        ]);

    	$author = new Author();

    	$author->setName($request->get('name'));

    	$author->save();

    	return redirect('/authors')
    		->with('message', 'El autor se creo correctamente');
    }

    public function edit(int $id): View
    {
        $author = Author::findOrFail($id);

        return view('authors.edit')
            ->with('author', $author);
    }

    public function update(Request $request, int $id): RedirectResponse
    {
        $author = Author::findOrFail($id);

        $author->setName($request->get('name'));
        $author->update();

        return redirect()
            ->to('/authors')
            ->with('message', 'El autor se actualizo correctamente');
    }

    public function destroy(int $id): RedirectResponse
    {
        $author = Author::findOrFail($id);

        $author->delete();

        return redirect()
            ->to('/authors')
            ->with('message', 'El autor se ha eliminado correctamente');
    }
}
