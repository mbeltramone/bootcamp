<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/* Authentication Routes */
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

/* Begin CRUD Authors  */

Route::get('/authors', 'AuthorsController@index');
Route::get('/authors/create', 'AuthorsController@create');
Route::post('/authors', 'AuthorsController@store');
Route::get('/authors/{id}/edit', 'AuthorsController@edit');
Route::put('/authors/{id}', 'AuthorsController@update');
Route::delete('/authors/{id}', 'AuthorsController@destroy');

/* End CRUD Authors  */

/* Begin CRUD Books  */

Route::get('/books', 'BooksController@index');
Route::post('/books', 'BooksController@store');
Route::get('/books/create', 'BooksController@create');

Route::get('/books/{id}/edit', 'BooksController@edit');
Route::put('/books/{id}', 'BooksController@update');
Route::delete('/books/{id}', 'BooksController@destroy');

/* End CRUD Books  */

/* Begin CRUD Reservations  */

Route::get('/reservations', 'ReservationsController@index');
Route::post('/reservations', 'ReservationsController@store');
Route::get('/reservations/create', 'ReservationsController@create');

Route::get('/reservations/{id}/edit', 'ReservationsController@edit');
Route::put('/reservations/{id}', 'ReservationsController@update');
Route::delete('/reservations/{id}', 'ReservationsController@destroy');

/* End CRUD Reservations  */
